import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ListComponent } from './list/list.component';
import { ViewComponent } from './view/view.component';

const routes: Routes = [
    {
        path: '',
        component: ListComponent,
        data: {
            title: 'Users List',
        },
    },
    {
        path: 'list',
        component: ListComponent,
        data: {
            title: 'Users List',
        }
    },
    {
        path: 'view',
        component: ViewComponent,
        data: {
            title: 'User Details',
        }
    },
    // {
    //     path: 'add',
    //     component: AddComponent,
    //     data: {
    //         title: 'Add Vendor',
    //     }
    // },
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class UserRoutingModule { }
