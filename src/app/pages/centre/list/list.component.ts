import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { CentreService } from '../../../shared/services/centre.service';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.scss']
})
export class ListComponent implements OnInit {

  rows = [];
  page = {
    recordPerPage: 10,
    totalRecords: 0,
    pages: 0,
    currentPage: 0,
    orderDir: '',
    orderBy: ''
  };
  searchObj: any = {};
  title: string = 'Centre List'
  constructor(
    private route: ActivatedRoute,
    private toastr: ToastrService,
    private centreService: CentreService
  ) {
    this.title = this.route.snapshot.data.title ? this.route.snapshot.data.title : this.title;
  }

  ngOnInit() {
    this.setPage({ offset: 0 });
  }

  setPage(page_info) {
    this.page.currentPage = page_info.offset;
    const reqObj: any = {
      recordPerPage: this.page.recordPerPage,
      pageNumber: this.page.currentPage + 1
    };
    if (this.searchObj.centre_name) {
      reqObj.centre_name = this.searchObj.centre_name;
    }
    if(this.page.orderBy && this.page.orderDir) {
      reqObj.orderBy = this.page.orderBy;
      reqObj.orderDir = this.page.orderDir;
    }
    this.centreService.getCentres(reqObj).subscribe(
      res => {
        if (res.success) {
          this.rows = res.result.data;
          this.page.totalRecords = res.result.totalRecords;
        } else {
          this.toastr.error(res.result);
        }
      },
      error => {
        this.toastr.error(error.error)
      }
    );
  }

  sortCallback(sortInfo: { sorts: { dir: string, prop: string }[], column: {}, prevValue: string, newValue: string }) {
    // there will always be one "sort" object if "sortType" is set to "single"
    console.log('sortInfo', sortInfo);
    this.page.orderDir = sortInfo.sorts[0].dir;
    this.page.orderBy = sortInfo.sorts[0].prop;
    this.setPage({ offset: 0 });
  }

  search(event) {
    if(!event.target.value.trim()) {
      return false;
    } else {
      this.setPage({ offset: 0 });
    }
  }

  delete(id) {
    Swal.fire({
      title: 'Are you sure, you want to delete this centre?',
      type: 'warning',
      showCancelButton: true,
      confirmButtonText: 'Yes'
    }).then(result => {
      if (result.value) {
        this.centreService.deleteCentre(id).subscribe((res) => {
          if (res.status) {
            this.toastr.success(res.result);
            this.rows = this.rows.filter(item => item.id !== id);
            this.page.totalRecords -= 1;
            if (!this.rows.length) {
              this.setPage({ offset: 0 });
            }
          }
        }, (error) => {

        })
      }
    });
  }

}
