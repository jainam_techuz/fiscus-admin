import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { CentreService } from '../../../shared/services/centre.service';

import { FormGroup, FormControl, Validators } from '@angular/forms';
import { CommonService } from '../../../shared/services/common.service';
import { ValidationService } from '../../../shared/services/validator.service';
import { NgbModal, ModalDismissReasons } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-view',
  templateUrl: './view.component.html',
  styles: []
})
export class ViewComponent implements OnInit {

  id: any;
  rows = [];
  page = {
    recordPerPage: 10,
    totalRecords: 0,
    pages: 0,
    currentPage: 0,
    orderDir: '',
    orderBy: ''
  };
  centre: any;
  title: string = 'Centre Details'
  centreUserForm: FormGroup;
  roles: any;
  closeResult: string;
  constructor(
    private route: ActivatedRoute,
    private toastr: ToastrService,
    private centreService: CentreService,
    private commonService: CommonService,
    private validationService: ValidationService,
    private modalService: NgbModal,
  ) {
    this.title = this.route.snapshot.data.title ? this.route.snapshot.data.title : this.title;
    this.id = this.route.snapshot.queryParamMap.get('id');
  }

  ngOnInit() {
    this.getCentre();
    this.setPage({ offset: 0 });

    // For Adding Centre User
    this.setForm();
    this.getRolesList();
  }

  getCentre() {
    this.centreService.getCentre(this.id).subscribe(
      res => {
        if (res.success) {
          this.centre = res.result;
        } else {
          this.toastr.error(res.result);
        }
      },
      error => {
        this.toastr.error(error.error)
      }
    );
  }

  setPage(page_info) {
    this.page.currentPage = page_info.offset;
    const reqObj: any = {
      recordPerPage: this.page.recordPerPage,
      pageNumber: this.page.currentPage + 1
    };
    reqObj.centre_id = this.id;
    if (this.page.orderBy && this.page.orderDir) {
      reqObj.orderBy = this.page.orderBy;
      reqObj.orderDir = this.page.orderDir;
    }
    this.centreService.getCentreUsers(reqObj).subscribe(
      res => {
        if (res.success) {
          this.rows = res.result.data;
          this.page.totalRecords = res.result.totalRecords;
        } else {
          this.toastr.error(res.result);
        }
      },
      error => {
        this.toastr.error(error.error)
      }
    );
  }

  sortCallback(sortInfo: { sorts: { dir: string, prop: string }[], column: {}, prevValue: string, newValue: string }) {
    // there will always be one "sort" object if "sortType" is set to "single"
    console.log('sortInfo', sortInfo);
    this.page.orderDir = sortInfo.sorts[0].dir;
    this.page.orderBy = sortInfo.sorts[0].prop;
    this.setPage({ offset: 0 });
  }

  open(content) {
    this.modalService.open(content, {ariaLabelledBy: 'modal-basic-title'}).result.then((result) => {
      this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
      this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
    });
  }

  private getDismissReason(reason: any): string {
    this.centreUserForm.reset();
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return  `with: ${reason}`;
    }
  }

  setForm() {
    this.centreUserForm = new FormGroup({
      role_id: new FormControl('', [Validators.required]),
      user_id: new FormControl('', [Validators.required]),
    });
  }

  getRolesList() {
    this.centreService.getRoles().subscribe(
      res => {
        if (res.success) {
          this.roles = res.result;
        } else {
          this.toastr.error(res.result);
        }
      },
      error => {
        this.toastr.error(error.error)
      }
    );
  }

  addUser() {
    if (!this.centreUserForm.valid) {
      this.validationService.validateAllFormFields(this.centreUserForm);
      return false;
    }
    this.centreUserForm.value.centre_id = this.id;
    const reqData: any = this.commonService.toFormData(this.centreUserForm.value);
    this.centreService.addCentreUser(reqData).subscribe(
      (response: any) => {
        if (response.status) {
          this.toastr.success(response.result);
          this.setPage({ offset: 0 });
        }
      },
      error => {
        if (error.statusCode === 400) {
          this.centreUserForm = this.commonService.setErrors(this.centreUserForm, error.error);
          this.validationService.validateAllFormFields(this.centreUserForm);
        }
      }
    );
  }

}