import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { ToastrService } from 'ngx-toastr';

import { CommonService } from '../../../shared/services/common.service';
import { ValidationService } from '../../../shared/services/validator.service';
import { CentreService } from '../../../shared/services/centre.service';

@Component({
  selector: 'app-add',
  templateUrl: './add.component.html',
  styleUrls: ['./add.component.scss']
})
export class AddComponent implements OnInit {

  regularForm: FormGroup;
  isEdit = false;
  id: string;
  title = 'Add Centre';
  centreData: any = {};
  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private toastr: ToastrService,
    private commonService: CommonService,
    private validationService: ValidationService,
    private centreService: CentreService,
  ) {
    this.title = this.route.snapshot.data.title ? this.route.snapshot.data.title : this.title;
    this.id = this.route.snapshot.queryParamMap.get('id');
  }

  ngOnInit() {
    if (this.id) {
      this.isEdit = true;
      this.getCentreData();
    }
    this.setForm();
  }

  getCentreData() {
    this.centreService.getCentre(this.id).subscribe(
      res => {
        if (res.success) {
          this.centreData = res.result;
          this.regularForm.patchValue(this.centreData);
        } else {
          this.toastr.error(res.result);
        }
      },
      error => {
        this.toastr.error(error.error)
      }
    );
  }

  setForm() {
    this.regularForm = new FormGroup({
      centre_name: new FormControl('', [Validators.required, Validators.maxLength(100)]),
      abn_number: new FormControl('', [Validators.required, Validators.maxLength(50)]),
      phone_number: new FormControl('', [Validators.required, Validators.maxLength(10)]),
      mobile_number: new FormControl('', [Validators.required, Validators.maxLength(10)]),
      email: new FormControl('', [Validators.required, Validators.email]),
      service_provider_number: new FormControl('', [Validators.required, Validators.maxLength(50)]),
      provider_approval_number: new FormControl('', [Validators.required, Validators.maxLength(50)]),
      license_capacity: new FormControl('', [Validators.required]),
    });
  }

  onReactiveFormSubmit() {
    if (!this.regularForm.valid) {
      this.validationService.validateAllFormFields(this.regularForm);
      return false;
    }
    if (this.isEdit) {
      this.regularForm.value.id = this.id;
      const reqData: any = this.commonService.toFormData(this.regularForm.value);
      this.centreService.upadteCentre(this.id, reqData).subscribe(
        (response: any) => {
          if (response.status) {
            this.toastr.success(response.result);
            this.router.navigate(['centre/list']);
          }
        },
        error => {
          if (error.statusCode === 400) {
            this.regularForm = this.commonService.setErrors(this.regularForm, error.error);
            this.validationService.validateAllFormFields(this.regularForm);
          }
        }
      );
    } else {
      const reqData: any = this.commonService.toFormData(this.regularForm.value);
      console.log(reqData, 'reqData for add'); return;
      this.centreService.addCentre(reqData).subscribe(
        (response: any) => {
          if (response.status) {
            this.router.navigate(['category/list']);
            this.toastr.success(response.result);

          }
          this.regularForm.reset();
        },
        error => {
          if (error.statusCode === 400) {
            this.regularForm = this.commonService.setErrors(this.regularForm, error.error);
            this.validationService.validateAllFormFields(this.regularForm);
          }
        }
      );
    }
  }

  goBack() {
    if (this.isEdit) {
      this.router.navigate(['centre/view'], { queryParams: { id: this.id } });
    } else {
      this.router.navigate(['centre/list']);
    }
  }

}
