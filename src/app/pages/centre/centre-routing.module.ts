import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ListComponent } from './list/list.component';
import { ViewComponent } from './view/view.component';
import { AddComponent } from "./add/add.component";

const routes: Routes = [
    {
        path: '',
        component: ListComponent,
        data: {
            title: 'Centre List',
        },
    },
    {
        path: 'list',
        component: ListComponent,
        data: {
            title: 'Centre List',
        }
    },
    {
        path: 'view',
        component: ViewComponent,
        data: {
            title: 'Centre Details',
        }
    },
    {
        path: 'add',
        component: AddComponent,
        data: {
            title: 'Add Centre',
        }
    },
    {
        path: 'edit',
        component: AddComponent,
        data: {
            title: 'Edit Centre',
        }
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class CentreRoutingModule { }
