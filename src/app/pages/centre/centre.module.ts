import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CentreRoutingModule } from "./centre-routing.module";
import { ViewComponent } from './view/view.component';
import { ListComponent } from './list/list.component';
import { NgxDatatableModule } from '@swimlane/ngx-datatable';
import { AddComponent } from './add/add.component';
import { SharedModule } from "../../shared/shared.module";

@NgModule({
  declarations: [ViewComponent, ListComponent, AddComponent],
  imports: [
    CommonModule,
    CentreRoutingModule,
    NgxDatatableModule,
    FormsModule,
    ReactiveFormsModule,
    SharedModule
  ]
})
export class CentreModule { }
