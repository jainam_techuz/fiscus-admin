import { Component } from '@angular/core';
import { FormGroup, Validators, FormBuilder } from '@angular/forms';
import { ToastrService } from 'ngx-toastr';
import { CommonService } from 'app/shared/services/common.service';
import { ValidationService } from "app/shared/services/validator.service";
import { ConstantService } from 'app/shared/services/constant.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html'
})

export class LoginComponent {
  toggleEye = "ft-eye-off";
  loginObj: any = {};
  loginForm: FormGroup;
  constructor(
    private toastr: ToastrService,
    private commonService: CommonService,
    private formBuilder: FormBuilder,
    private validationService: ValidationService,
    public constant: ConstantService,
  ) { }

  ngOnInit() {
    this.setLoginForm();
  }

  setLoginForm() {
    this.loginForm = this.formBuilder.group({
      email: ['', [Validators.required, Validators.email]],
      password: ['', [Validators.required, Validators.minLength(6)]]
    });
  }

  onSubmit() {
    if (!this.loginForm.valid) {
      this.validationService.validateAllFormFields(this.loginForm);
      return false;
    }
    this.loginForm.value.is_admin = 1;
    this.commonService.login(this.loginForm.value).subscribe(
      (response: any) => {
        if (response.status) {
          window.localStorage['token'] = response.result.token.accessToken;
          this.commonService.goToHome();
        }
        this.loginForm.reset();
      },
      error => {
        this.toastr.error(error.error);
      }
    );
  }

  togglePassword(event, oldPassword: any) {
    this.toggleEye = this.toggleEye == "ft-eye" ? "ft-eye-off" : "ft-eye" ;
    oldPassword.type = oldPassword.type === 'password' ? 'text' : 'password';
  }
}