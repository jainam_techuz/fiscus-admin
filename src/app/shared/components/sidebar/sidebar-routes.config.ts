import { RouteInfo } from './sidebar.metadata';

export const ROUTES: RouteInfo[] = [
    {
        path: '/centre/list', title: 'Centre', icon: 'fa fa-contao', class: '', badge: '', badgeClass: '', isExternalLink: false, submenu: []
    },
    {
        path: '/user/list', title: 'Users', icon: 'fa fa-users', class: '', badge: '', badgeClass: '', isExternalLink: false, submenu: []
    },
];
