import { Routes, RouterModule } from '@angular/router';
import { PageGuard } from '../guards/page-guard.service';

//Route for content layout with sidebar, navbar and footer
export const Full_ROUTES: Routes = [
  {
    path: 'centre',
    loadChildren: () => import('../../pages/centre/centre.module').then(m => m.CentreModule),
    canActivateChild: [PageGuard]
  },
  {
    path: 'user',
    loadChildren: () => import('../../pages/user/user.module').then(m => m.UserModule),
    canActivateChild: [PageGuard]
  },
];
