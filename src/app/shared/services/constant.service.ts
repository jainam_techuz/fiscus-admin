import { Injectable } from '@angular/core';
import { environment } from '../../../environments/environment';
@Injectable({
  providedIn: 'root'
})
export class ConstantService {
  apiUrl = environment.api_url;

  NOT_FOUND = 'Data not found!';
  NOT_ALLOWED = 'Not allowed to access!';
  FEILD_REQUIRED = 'Field is required.';
  INVALID_ADDRESS = 'Unfortunately, this address wasn\'t found. Please double-check it and try again.';
  VALID_EMAIL = 'Must be valid email.';
  MAX_FILE_5 = 'Maximum 5 files allowed.';
  MAX_FILE_SIZE_5 = 'Maximum file size to upload is 5MB.';
  constructor() { }

}
