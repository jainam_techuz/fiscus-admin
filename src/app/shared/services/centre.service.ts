import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Router } from '@angular/router';
import { environment } from '../../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class CentreService {
  apiUrl = environment.api_url;
  constructor(
    public http: HttpClient,
    public router: Router,
  ) { }

  // Centre

  addCentre(data) {
    return this.http.post<any>(`${this.apiUrl}/centres`, data);
  }

  getCentres(params?) {
    return this.http.get<any>(`${this.apiUrl}/centres`, { params });
  }

  getCentre(id) {
    return this.http.get<any>(`${this.apiUrl}/centres/${id}`);
  }

  upadteCentre(id, data) {
    return this.http.put<any>(`${this.apiUrl}/centres/${id}`, data);
  }

  deleteCentre(id) {
    return this.http.delete<any>(`${this.apiUrl}/centres/${id}`);
  }

  // Roles
  getRoles() {
    return this.http.get<any>(`${this.apiUrl}/roles`);
  }


  // Centre Users

  addCentreUser(data) {
    return this.http.post<any>(`${this.apiUrl}/centre_users`, data);
  }

  getCentreUsers(params?) {
    return this.http.get<any>(`${this.apiUrl}/centre_users`, { params });
  }

}
