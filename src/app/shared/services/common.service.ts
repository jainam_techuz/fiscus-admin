import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Router } from '@angular/router';
import * as jwt_decode from 'jwt-decode';

import { environment } from '../../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class CommonService {
  apiUrl = environment.api_url;
  constructor(
    public http: HttpClient,
    public router: Router,
  ) { }
  logout() {
    localStorage.clear();
    this.router.navigate(['/auth/login']);
  }
  login(data): Observable<any> {
    return this.http.post<any>(`${this.apiUrl}/auth/login`, data);
  }

  objectToFormData(data = {}, form = null, namespace = '') {
    console.log('-----data-', data);
    let files = {};
    let model = {};
    for (let propertyName in data) {
      console.log('propertyName 1---', propertyName)
      if (data.hasOwnProperty(propertyName) && data[propertyName] instanceof File) {
        files[propertyName] = data[propertyName]
      } else {
        model[propertyName] = data[propertyName]
      }
    }

    // model = JSON.parse(JSON.stringify(model))
    let formData = form || new FormData();

    for (let propertyName in model) {
      if (!model.hasOwnProperty(propertyName) || !model[propertyName]) continue;
      let formKey = namespace ? `${namespace}[${propertyName}]` : propertyName;
      if (model[propertyName] instanceof Date) {
        console.log('propertyName 2---', propertyName)
        formData.append(formKey, model[propertyName].toISOString());
      } else if (model[propertyName] instanceof File) {
        console.log('propertyName 3---', propertyName)
        formData.append(formKey, model[propertyName]);
      } else if (model[propertyName] instanceof Array) {
        console.log('propertyName 4---', propertyName)
        model[propertyName].forEach((element, index) => {
          const tempFormKey = `${formKey}[${index}]`;
          console.log('propertyName 4.1---', propertyName, typeof element, element, tempFormKey);
          if (element instanceof File) {
            formData.append(tempFormKey, element);
          } else if (typeof element === 'object') {
            this.objectToFormData(element, formData, tempFormKey);
          } else {

            formData.append(tempFormKey, element.toString());
          }
        });
      } else if (typeof model[propertyName] === 'object' && !(model[propertyName] instanceof File)) {
        console.log('propertyName 5---', propertyName)
        this.objectToFormData(model[propertyName], formData, formKey);
      } else {
        console.log('propertyName 6---', propertyName)
        formData.append(formKey, model[propertyName].toString());
      }
    }

    for (let propertyName in files) {
      if (files.hasOwnProperty(propertyName)) {
        formData.append(propertyName, files[propertyName]);
      }
    }
    return formData;
  };
  toFormData<T>(formValue: T) {
    const formData = new FormData();
    for (const key of Object.keys(formValue)) {
      const value = formValue[key];
      formData.append(key, value);
    }
    return formData;
  }

  setErrors(form, errorData) {
    for (const [key, value] of Object.entries(errorData)) {
      if (form.controls[key]) {
        form.controls[key].setErrors({
          serverError: value
        });
      }
    }
    return form;
  }

  goToHome() {
    const token = localStorage.getItem('token');
    if (token) {
      return this.router.navigate(['/user']);
    }
    return this.logout();
  }

  getLoggedInUser() {
    const token = localStorage.getItem('token');
    if (token) {
      return this.getDecodedAccessToken(token);
    }
    return;
  }

  getDecodedAccessToken(token: string): any {
    try {
      return jwt_decode(token);
    } catch (Error) {
      return null;
    }
  }

  // CHECK THE USER role, RETURN TRUE IF USER HAS valid role
  IsUserHasRight(allowedRoles: number[]) {
    const loggedInUser = this.getLoggedInUser();
    return loggedInUser && loggedInUser.role && allowedRoles.indexOf(Number(loggedInUser.role)) !== -1 ? true : false;
  }
}